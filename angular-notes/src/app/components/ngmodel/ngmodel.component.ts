import {Component, OnInit, ViewChild} from '@angular/core';
import {DCheroEvents} from "../../models/DCheroEvents";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-ngmodel',
  templateUrl: './ngmodel.component.html',
  styleUrls: ['./ngmodel.component.css']
})
export class NgmodelComponent implements OnInit {

  // Properties
  // a property for the new character entry
  // - set each property of the object to empty/null value
  character: DCheroEvents ={
    persona: '',
    firstName: '',
    lastName: '',
    age: null,
    address: {
      street: '',
      city: '',
      state: '',
    },
    isActive: null,
    memberSince: '',
  }

  characters: DCheroEvents[];
  enableAddUser: boolean;
  currentClasses: {};
  currentStyle: {};

  showForm: boolean;

  // being used to subscribe to our data
  data: any;

  // SUBMIT LESSON
  @ViewChild('characterForm')form: any;
  /*
  Calling ViewChild and passed in the name of our form 'characterForm'
  *making this our "Form Identifier"
   */

  // inject the data service in the constructor
  constructor(private dataService: DataService) { }
  /*
  private = can't be used anywhere else, only within this class
  dataService = variable
  DataService = setting our variable to the DataService
  that we brought in
  Now we should be able to access any methods inside of
  our DataService
   */


  ngOnInit(): void {
    this.enableAddUser = true;
    this.showForm = false;
    // adding data of array of characters
    // this.characters = [
    //  {
    //   persona: 'Superman',
    //   firstName: 'Clark',
    //   lastName: 'Kent',
    //   age: 34,
    //   address: {
    //     street: '27 Smallville',
    //     city: 'Metropolis',
    //     state: 'IL'
    //   },
    //   img: '../../assets/img/superman.jpg',
    //   isActive: true,
    //   balance: 123000,
    //   memberSince: new Date('05/01/1939 8:30:00'),
    //   hide: false
    // },
    //   {
    //     persona: 'Raven',
    //     firstName: 'Rachel',
    //     lastName: 'Roth',
    //     age: 134,
    //     address: {
    //       street: '1600 Main St',
    //       city: 'Los Angeles',
    //       state: 'CA'
    //     },
    //     img: '../../assets/img/raven.jpg',
    //     hide: false
    //   },
    //   {
    //     persona: 'Batman',
    //     firstName: 'Bruce',
    //     lastName: 'Wayne',
    //     age: 43,
    //     address: {
    //       street: '50 Wayne Manor',
    //       city: 'Gotham',
    //       state: 'NY'
    //     },
    //     img: '../../assets/img/batman.jpg',
    //     isActive: true,
    //     memberSince: new Date('01/01/1937 8:30:00'),
    //     hide: false
    //   }] // end or array

    this.setCurrentClasses();
    this.setCurrentStyle();


    // access the getCharacters() that's inside our DataService
    // this.characters = this.dataService.getCharacters()

    // refactor due to our observable in our dataService
    // this.characters = this.dataService.getCharacters()
    this.dataService.getCharacters().subscribe(c => {
      this.characters = c;
    })

    // Subscribing to the Observable
    // 1. how can I access getData() inside of DataService
    // this.dataService.getData()
    // 2. subscribe to our observable
    this.dataService.getData().subscribe(data => {
      console.log(data)
    })



  }

  // Method
  // ngClass
  setCurrentClasses() {
    this.currentClasses = {
      'btn-success': this.enableAddUser
    }
  }

  // ngStyle
  setCurrentStyle() {
    this.currentStyle ={
      'padding-top': '60px',
      'text-decoration': 'underline'
    }
  }

  toggleForm() {
    this.showForm = !this.showForm
  }

  toggleInfo(character) {
    // alert('button was clicked');
    character.hide = !character.hide
  }

  fireEvent(e) {
    console.log(e.type);
    console.log(e.target.value)
  }

  onSubmit({value, valid}: {value: DCheroEvents, valid: boolean}) {

    if (!valid) {
      console.log('Form is not valid')
    }
    else {
      value.isActive = true;
      value.memberSince = new Date();
      value.hide = false;
    }

    // this.characters.unshift(value)
    this.dataService.addCharacter(value);
    // fetching our characters from the service and adding it through the service
    // ** only working because we're working with synchronous

    this.form.reset();
  }

  // passing in an object as our method's argument
  // value set as a DCheroEvents Type
  // valid set as a boolean Type

  // add new character
  // addCharacter() {
  //   // what we want to do?
  //   this.characters.unshift(this.character);
  //
  //   // clear out the form / reset the form
  //   this.character = {
  //     persona: '',
  //     firstName: '',
  //     lastName: '',
  //     age: null,
  //     address: {
  //       street: '',
  //       city: '',
  //       state: '',
  //     },
  //     isActive: ,
  //     memberSince: '',
  //   }
  // }

  // methods for the type of events
  triggerEvent(e) {
    console.log(e.type)
  }

}
