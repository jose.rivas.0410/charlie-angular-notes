export interface DChero {
  persona: string,
  firstName: string,
  lastName: string,
  age: number,
  address?: {
    street: string,
    city: string,
    state: string,
  },
  img?: string,
  isActive?: boolean,
  balance?: number,
  memberSince?: any,
}
