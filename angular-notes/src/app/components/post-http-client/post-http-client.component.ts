import { Component, OnInit } from '@angular/core';
import { PostHttpClient } from "../../models/PostHttpClient";
import { PostService } from "../../services/post.service";

@Component({
  selector: 'app-post-http-client',
  templateUrl: './post-http-client.component.html',
  styleUrls: ['./post-http-client.component.css']
})
export class PostHttpClientComponent implements OnInit {
  // property
  posts: PostHttpClient[];

  currentPost: PostHttpClient = {
    id: 0,
    title: '',
    body: ''
  };

  // property for our update button
  isEdit: boolean = false;

  // inject our service as a dependency
  // Dependency Injection
  constructor(private postService: PostService) { }

  // fetch the post when ngOnInit() method is initialized
  ngOnInit(): void {
    // subscribe our Observable that's in our PostService
    this.postService.getPost().subscribe(p => {
      console.log(p)
      this.posts = p;
    })
  }

  // create onNewPost() method
  onNewPost(post: PostHttpClient) {
    this.posts.unshift(post);

  }

  // create method for onUpdatePost()
  onUpdatedPost(post: PostHttpClient) {
    this.posts.forEach((current, index) => {
      if (post.id === current.id) {
        this.posts.splice(index, 1);
        this.posts.unshift(post);
        this.isEdit = false;
        this.currentPost = {
          id: 0,
          title: '',
          body: ''
        }
      }
    })
  }

  // create editPost() method
  editPost(post: PostHttpClient) {
    this.currentPost = post;

    // included for the update button
    this.isEdit = true;
  }

  // create the method for removePost()
  removePost(post: PostHttpClient) {
    if (confirm('Are you sure?')) {
      this.postService.deletePost(post.id).subscribe(() =>{
        this.posts.forEach((current, index) => {
          if (post.id === current.id) {
            this.posts.splice(index, 1);
          }
        })
      })
    }
  }

}
