import { Component, OnInit } from '@angular/core';
import {User} from "../../models/User";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  // PROPERTIES - an attribute of a component
  // goes inside of the class
  // firstName = 'Charlie';
  // lastName = 'Cohort';
  // age = 33;

  // creating property name user, referencing our interface User
  users: User[];


  constructor() {
    console.log("Hello from Users Component...");

    // connect with our interface


    // this.greeting();
  }

  ngOnInit(): void {
    this.users = [{
      firstName: 'Clark',
      lastName:'Kent',
      age: 65
    },
      {
        firstName: 'Bruce',
        lastName:'Wayne',
        age: 43
      }];
  }

//  METHODS - function inside of a component's class
//   greeting() {
//     return "Hello there, " + this.users[0].firstName + ' ' + this.users[1].lastName;
//     // in order to access properties OR methods, you need to use the 'this' keyword.
//   }

}
