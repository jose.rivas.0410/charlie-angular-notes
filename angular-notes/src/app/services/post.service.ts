import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { PostHttpClient } from "../models/PostHttpClient";


// send a header value of the content type
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PostService {
  // property
  // set a url as a property
  postURL: string = 'https://jsonplaceholder.typicode.com/posts';

  // inject the httpclient as a dependency
  constructor(private http: HttpClient) { }

  // create a method that will make our GET request
  getPost() : Observable<PostHttpClient[]> {
    // return all the data that comes with our postURL property
    return this.http.get<PostHttpClient[]>(this.postURL);
  }


  // create a method for our POST request
  savePost(post: PostHttpClient) : Observable<PostHttpClient> {
    return this.http.post<PostHttpClient>(this.postURL, post, httpOptions)
  }

  // create a method for our PUT request
  updatePost(post: PostHttpClient) : Observable<PostHttpClient> {
    const url = `${this.postURL}/${post.id}`;

    return this.http.put<PostHttpClient>(url, post, httpOptions)

  }
  /*
  1. Created a new const 'url' - this is taking the json url / id of the post
  2. Returning everything as a PUT request, not a POST
  3. "PUTTING" this on the url, NOT the this.url
   */

  // create a method for deletePost()
  deletePost(post: PostHttpClient | number) : Observable<PostHttpClient> {
    const id = typeof post === 'number' ? post : post.id;
    // if what's passed in here is a number,
    // it's post, otherwise use id of 'post'

    const url = `${this.postURL}/${id}`;

    return this.http.delete<PostHttpClient>(url, httpOptions)
  }


}
