export interface Athlete {
  name: string,
  team: string,
  jerseyNu: number,
  isActive: boolean,
}
