import { Component, OnInit } from '@angular/core';
import {Movie} from "../../models/Movie";

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  movies: Movie[];
  enableCheckMovie: boolean;
  colorPG: {};
  colorPG13: {};
  colorR: {};

  constructor() {
    this.enableCheckMovie = true;

    this.movies= [{
      title: 'Monty Python and the Holy Grail',
      yearReleased: new Date('05/25/1975'),
      actors: ['Graham Chapman', ' John Cleese', ' Eric Idle', ' Terry Gilliam',
        ' Terry Jones', ' Michael Palin', ' Connie Booth', ' Carol Cleveland',
        ' Neil Innes', ' Bee Duffell', ' John Young', ' Rita Davis', ' Avril Stewart',
        ' Sally Kinghorn', ' Mark Zycon'],
      directors: ['Terry Gilliam', ' Terry Jones'],
      genre: ['Adventure', ' Comedy', ' Fantasy'],
      rating: 'PG',
      awards: ['European Film Awards', ' Online Film & Television Association'],
      img: '../../assets/img/monty-python-holy-grail.jpg',
      balance:1938974,
    },
      {
        title: 'Scary Movie IV',
        yearReleased: new Date('04/14/2006'),
        actors: ['Anna Faris', ' Regina Hall', ' Craig Bierko', ' Bill Pullman',
          ' Anthony Anderson', ' Leslie Nielsen', ' Molly Shannon', ' Michael Madsen',
          ' Chris Elliot', ' Carmon Electra', " Shaquille O'Neal", ' Phil McGraw',
          ' Cloris Leachman', ' Conchita Campbell', ' Beau Mirchoff'],
        directors: 'David Zucker',
        genre: ['Comedy', ' Horror'],
        rating: 'PG-13',
        awards: ['Worst Supporting Actress'],
        img: '../../assets/img/scary-movie-iv.jpg',
        balance: 178262620,
      },
      {
        title: 'Austin Powers: International Man of Mystery',
        yearReleased: new Date('05/02/1997'),
        actors: ['Mike Myers', ' Elizabeth Hurley', ' Michael York', ' Mimi Rogers',
          ' Robert Wagner', ' Seth Green', ' Fabiana Udenio', ' Mindy Sterling',
          ' Paul Dillon', ' Charles Napier', ' Will Ferrell', ' Joann Richter',
          ' Anastasia Sakelaris', ' Afifi Alaouie', ' Monet Mazur'],
        directors: 'Jay Roach',
        genre: ['Adventure', ' Comedy'],
        rating: 'PG-13',
        awards: ['Best Fantasy Film', ' Best Villain', ' Best Dance Sequence'],
        img: '../../assets/img/austin-powers-imom.jpg',
        balance:67683989,
      },
      {
        title: 'A Haunted House 2',
        yearReleased: new Date('04/18/2014'),
        actors: ['Marlon Wayans', ' Jaime Pressly', ' Essence Atkins', ' Gabriel Iglesias',
          ' Missi Pyle', ' Ashley Rickards', ' Affion Crockett', ' Steele Stebbins',
          ' Rick Overton', ' Hayes MacArthur', ' Dave Sheridan', ' Cedric the Entertainer',
          ' Kurt Carley', ' Tom Virtue', ' Kym Whitley'],
        directors: 'Michael Tiddes',
        genre: ['Comedy', ' Fantasy'],
        rating: 'R',
        awards: ['None'],
        img: '../../assets/img/a-haunted-house-two.jpg',
        balance:25358716,
      }]

    this.setColorPG()
  }

  ngOnInit(): void {
  }

  ratingG() {
    return 'This movie is family friendly and can be watched by any age'
  }

  ratingPG() {
    return 'This movie urges parental guidance since this film may contain material that may not be appropriate for all ages'
  }

  ratingPG13() {
    return 'This film contains inappropriate material for pre-teens and parents are urged to be cautious'
  }

  ratingR() {
    return 'This film contains mature material that may not be appropriate for those under 18 years of age'
  }

  setColorPG() {
    this.colorPG = {
      'btn-success': this.enableCheckMovie
    }
    this.colorPG13 = {
      'btn-warning': this.enableCheckMovie
    }
    this.colorR = {
      'btn-danger': this.enableCheckMovie
    }
  }


}
