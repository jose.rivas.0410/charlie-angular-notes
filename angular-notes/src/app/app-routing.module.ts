import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from "./components/home/home.component";
import { MoviesComponent } from "./components/movies/movies.component";
import {NgmodelComponent} from "./components/ngmodel/ngmodel.component";
import {PostHttpClientComponent} from "./components/post-http-client/post-http-client.component";

// this is where we're going to route our paths
const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'characters',
    component: NgmodelComponent
  },
  {
    path: 'movies',
    component: MoviesComponent
  },
  {
    path: 'posts',
    component: PostHttpClientComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
