export interface Bands {
  name: string,
  yearEstablished: number,
  vocalist: string,
  guitarist: any,
  bass: string,
  drums: string,
  albums: any,
}

