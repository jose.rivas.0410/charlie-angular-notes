import { Injectable } from '@angular/core';
import {DCheroEvents} from "../models/DCheroEvents";
import { Observable, of } from "rxjs";
/*
OBSERVABLES
- "data stream"
- we can subscribe and we can create them
 */
// import { of } from 'rxjs/observable/of'
/*
this will allow us to return an array as an observable
 */

@Injectable({
  providedIn: 'root'
})
export class DataService {

  // initialize the characters property and assign it in the array of our interface
  characters: DCheroEvents[];

  // property of our observable
  data: Observable<any>

  constructor() {
    this.characters = [{
      persona: 'Superman',
      firstName: 'Clark',
      lastName: 'Kent',
      age: 34,
      address: {
        street: '27 Smallville',
        city: 'Metropolis',
        state: 'IL'
      },
      img: '../../assets/img/superman.jpg',
      isActive: true,
      balance: 123000,
      memberSince: new Date('05/01/1939 8:30:00'),
      hide: false
    },
      {
        persona: 'Raven',
        firstName: 'Rachel',
        lastName: 'Roth',
        age: 134,
        address: {
          street: '1600 Main St',
          city: 'Los Angeles',
          state: 'CA'
        },
        img: '../../assets/img/raven.jpg',
        isActive: false,
        memberSince: new Date('05/19/1939 8:30:00'),
        hide: false
      },
      {
        persona: 'Batman',
        firstName: 'Bruce',
        lastName: 'Wayne',
        age: 43,
        address: {
          street: '50 Wayne Manor',
          city: 'Gotham',
          state: 'NY'
        },
        img: '../../assets/img/batman.jpg',
        isActive: true,
        memberSince: new Date('01/01/1937 8:30:00'),
        hide: false
      }
    ]; // end of array



  }


  // create a method that will return a type of DCheroEvents[]
  // getCharacters(): DCheroEvents[] {
  //   return this.characters;
  // }

  // redo our method to return an array as an observable
  getCharacters(): Observable<DCheroEvents[]> {
    console.log('Getting characters from dataService')
    return of(this.characters);
  }
  // this will break the code
  // because we're dealing with asynchronous data
  // now need to refactor our ngmodel.component.ts


  addCharacter(character: DCheroEvents) {
    this.characters.unshift(character)
  }


  // create a method, subscribe to ur observable
  getData() {
    this.data = new Observable(observer => {
      setTimeout(() => {
        observer.next(1);
      }, 1000);

      setTimeout(() => {
        observer.next(2);
      }, 2000);

      setTimeout(() => {
        observer.next(3);
      }, 3000)

      setTimeout(() => {
        observer.next(4);
      }, 4000)
    });
    return this.data
  }
}
