export interface Movie {
  title: string,
  yearReleased: any,
  actors: any,
  directors: any,
  genre: any,
  rating: string,
  awards: any,
  img?: string,
  balance?: number,
}
