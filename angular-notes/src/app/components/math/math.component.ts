import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {
  // ok to use but practical / local type
  // num1 = 10;
  // num2 = 20;

  // more practical / global type
  // nameOfPractical: type
  num1: number;
  num2: number;


  constructor() {
    this.sum(10,20);
    this.difference(10, 20);
    this.product(10, 20);
    this.quotient(10, 20);
    // this.bonus();
  }

  ngOnInit(): void {
  }

  sum(a, b) {
    this.num1 = a
    this.num2 = b
    return this.num1 + this.num2;
  }

  difference(a, b) {
    this.num1 = a
    this.num2 = b
    return this.num1 - this.num2;
  }

  product(a, b) {
    this.num1 = a
    this.num2 = b
    return this.num1 * this.num2;
  }

  quotient(a, b) {
    this.num1 = a
    this.num2 = b
    return this.num1 / this.num2;
  }

  // bonus() {
  //   for ( let i = 1; i <= 100; i++) {
  //     if (i % 3 === 0 && i % 5 === 0) {
  //       console.log("FIZZBUZZ");
  //     }
  //     else if (i % 3 === 0) {
  //       console.log("FIZZ");
  //     }
  //     else if (i % 5 === 0) {
  //       console.log("BUZZ");
  //     }
  //     else {
  //       console.log(i);
  //     }
  //   }
  // }

}
