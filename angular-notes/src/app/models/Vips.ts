export interface Vips {
  firstName: string,
  lastName: string,
  age: number,
  status: string
  username: string,
  memberNo: number,
}
