import { Component, OnInit } from '@angular/core';
import {Athlete} from "../../models/Athlete";

@Component({
  selector: 'app-string-interpolation',
  templateUrl: './string-interpolation.component.html',
  styleUrls: ['./string-interpolation.component.css']
})
export class StringInterpolationComponent implements OnInit {
  // properties
  athletes: Athlete[]; // pointing to our Athlete interface

  constructor() {
    this.athletes = [{
      name: 'Michael Jordan',
      team: 'Chicago Bulls',
      jerseyNu: 23,
      isActive: false,
    },
      {
        name: 'Lebron James',
        team: 'Los Angeles Lakers',
        jerseyNu: 32,
        isActive: true,
      }
      ]; // End of this.athlete array

    // CALLING OUR addAthlete() method inside of our constructor
    // and filled in the requirements for our Athlete interface
    this.addAthlete({
      name: 'Stephen Curry',
      team: 'Golden State Warriors',
      jerseyNu: 30,
      isActive: true,
    })
  }

  ngOnInit(): void {

  }

  // Method
  showStatus() {
      return `This player is still currently playing`
  }

  // Create a method 'addAthlete' that takes in an athlete as an Athlete type
  // and adds it as a new athlete to the array
  addAthlete(athlete: Athlete) {
    console.log('addAthlete is connected...');
    this.athletes.push(athlete);
  }

}
