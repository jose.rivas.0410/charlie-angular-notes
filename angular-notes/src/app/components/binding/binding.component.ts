import { Component, OnInit } from '@angular/core';
import {DChero} from "../../models/DChero";

@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent implements OnInit {

  // Properties
  characters: DChero[];
  enableAddUser: boolean;
  currentClasses: {}; // an empty object
  currentStyle: {}; // empty object

  constructor() { }

  ngOnInit(): void {
    this.enableAddUser = true;
    // adding data of array of characters
    this.characters = [{
      persona: 'Superman',
      firstName: 'Clark',
      lastName: 'Kent',
      age: 34,
      address: {
        street: '27 Smallville',
        city: 'Metropolis',
        state: 'IL'
      },
      img: '../../assets/img/superman.jpg',
      isActive: true,
      balance: 123000,
      memberSince: new Date('05/01/1939 8:30:00')
    },
      {
        persona: 'Raven',
        firstName: 'Rachel',
        lastName: 'Roth',
        age: 134,
        address: {
          street: '1600 Main St',
          city: 'Los Angeles',
          state: 'CA'
        },
        img: '../../assets/img/raven.jpg'
      },
      {
        persona: 'Batman',
        firstName: 'Bruce',
        lastName: 'Wayne',
        age: 43,
        address: {
          street: '50 Wayne Manor',
          city: 'Gotham',
          state: 'NY'
        },
        img: '../../assets/img/batman.jpg',
        isActive: true,
        memberSince: new Date('01/01/1937 8:30:00')
      }] // end or array

    this.setCurrentClasses();
    this.setCurrentStyle()
  }

  // Methods
  // ngClass
  setCurrentClasses() {
    this.currentClasses = {
      'btn-success': this.enableAddUser
    }
  }

  // ngStyle
  setCurrentStyle() {
    this.currentStyle ={
      'padding-top': '60px',
      'text-decoration': 'underline'
    }
  }

}
