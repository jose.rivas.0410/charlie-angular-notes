import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


// ANGULAR MATERIAL COMPONENTS
import { AppComponent } from './app.component';
import { UserComponent } from "./components/user.component";
import { HelloComponent } from "./components/hello/hello.component";
import { TravelComponent } from "./components/travel/travel.component";
import { UsersComponent } from './components/users/users.component';
import { MathComponent } from './components/math/math.component';
import { VipComponent } from './components/vip/vip.component';
import { StringInterpolationComponent } from './components/string-interpolation/string-interpolation.component';
import { BandsComponent } from './components/bands/bands.component';
import { MoviesComponent } from './components/movies/movies.component';
import { MaterialComponent } from './components/material/material.component';
import { BindingComponent } from './components/binding/binding.component';
import { EventsComponent } from './components/events/events.component';
import { NgmodelComponent } from './components/ngmodel/ngmodel.component';
import { PostHttpClientComponent } from './components/post-http-client/post-http-client.component';
import { PostFormComponent } from './components/post-form/post-form.component';

// ANGULAR MATERIAL MODULES
import { MatSliderModule } from "@angular/material/slider";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatNativeDateModule } from "@angular/material/core";
import { MatRadioModule } from "@angular/material/radio";
import { MatGridListModule } from "@angular/material/grid-list";
import { FormsModule } from "@angular/forms";
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';


@NgModule({
  declarations: [
    // COMPONENTS GO HERE
    AppComponent,
    UserComponent,
    HelloComponent,
    TravelComponent,
    UsersComponent,
    MathComponent,
    VipComponent,
    StringInterpolationComponent,
    BandsComponent,
    MoviesComponent,
    MaterialComponent,
    BindingComponent,
    EventsComponent,
    NgmodelComponent,
    PostHttpClientComponent,
    PostFormComponent,
    HomeComponent,
    NavbarComponent
  ],
  imports: [
    // MODULES GO HERE
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    MatSelectModule,
    MatNativeDateModule,
    MatRadioModule,
    MatGridListModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [], // SERVICES GO HERE
  bootstrap: [AppComponent]
})
export class AppModule { }
