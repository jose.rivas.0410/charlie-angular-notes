import { Component, OnInit } from '@angular/core';
import {Bands} from "../../models/Bands";

@Component({
  selector: 'app-bands',
  templateUrl: './bands.component.html',
  styleUrls: ['./bands.component.css']
})
export class BandsComponent implements OnInit {

  bands: Bands[];

  constructor() {
    this.bands = [{
      name: 'Imagine Dragons',
      yearEstablished: 2008,
      vocalist: 'Dan Reynolds',
      guitarist: 'Wayne Sermon',
      bass: 'Ben McKee',
      drums: 'Daniel Platzman',
      albums: ['Night Visions', ' Smoke + Mirrors', ' Evolve', ' Origins'],
    },
      {
        name: 'Foo Fighters',
        yearEstablished: 1994,
        vocalist: 'Dave Grohl',
        guitarist: ['Dave Grohl', ' Pat Smear', ' Chris Shiflett'],
        bass: 'Nate Mendel',
        drums: 'Taylor Hawkins',
        albums: ['Foo Fighters', ' The Colour and the Shape',
          ' There Is Nothing Left to Lose', ' One by One', ' In Your Honor',
          ' Echoes, Silence, Patience & Grace', ' Wasting Light', ' Sonic Highways',
          ' Concrete and Gold'],
      },
      {
        name: 'Nirvana',
        yearEstablished: 1987,
        vocalist: 'Kurt Cobain',
        guitarist: 'Kurt Cobain',
        bass: 'Krist Novoselic',
        drums: 'Dave Grohl',
        albums: ['Bleach', ' Nevermind', ' In Utero', ' Incesticide', ' Nirvana',
          ' Sliver: The Best of the Box', ' Icon'],
      },
      {
        name: 'Kiss',
        yearEstablished: 1973,
        vocalist: 'Paul Stanley',
        guitarist: 'Tommy Thayer',
        bass: 'Gene Simmons',
        drums: 'Eric Singer',
        albums: ['Kiss', ' Hotter Than Hell', ' Dressed to Kill', ' Destroyer',
          ' Rock and Roll Over', ' Love Gun', ' Dynasty', ' Unmasked',
          " Music from 'The Elder'", ' Creatures of the Night', ' Lick it Up',
          ' Animalize', ' Asylum', ' Crazy Nights', ' Hot in the Shade', ' Revenge',
          ' Carnival of Souls', ' Psycho Circus', ' Sonic Boom', ' Monster'],
      }]

    this.addBand({
      name: 'The Dudes',
      yearEstablished: 2019,
      vocalist: 'Dude 1',
      guitarist: 'Dude 2',
      bass: 'Dude 3',
      drums: 'Dude 4',
      albums: ['The Dudes', ' The Bros', ' May the Stars Fight for Us'],
    })
  }

  ngOnInit(): void {

  }

  addBand(band: Bands) {
    this.bands.push(band);
  }

}
