import { Component, OnInit } from '@angular/core';
import {Vips} from "../../models/Vips";

@Component({
  selector: 'app-vip',
  templateUrl: './vip.component.html',
  styleUrls: ['./vip.component.css']
})
export class VipComponent implements OnInit {

  vips: Vips[];

  constructor() {
    console.log("Hello from vip component")
  }

  ngOnInit(): void {
    this.vips = [{
      firstName: 'John',
      lastName: 'Doe',
      age: 38,
      status: 'Active',
      username: 'jodoe@email.com',
      memberNo: 1
    },
    {
      firstName: 'Jane',
      lastName: 'Doe',
      age: 37,
      status: 'Active',
      username: 'jadoe@gmail.com',
      memberNo: 2
    },
    {
      firstName: 'James',
      lastName: 'Kirk',
      age: 43,
      status: 'In-Active',
      username: 'jamks@email.com',
      memberNo: 3
    },
    {
      firstName: 'Amy',
      lastName: 'Whitter',
      age: 26,
      status: 'Active',
      username: 'awhitt@email.com',
      memberNo: 4
    },
    {
      firstName: 'Thomas',
      lastName: 'Gothenburg',
      age: 54,
      status: 'In-Active',
      username: 'thomas.g@email.com',
      memberNo: 5
    }]
  }

  showMemberNum () {
    return `There are 5 members in the VIP`
  }

}
