export interface User {
  firstName: string,
  lastName: string,
  age: number,
  email?: string,
  image?: string,
  itsActive?: boolean,
  balance?: number,
  memberSince?: any,
}
