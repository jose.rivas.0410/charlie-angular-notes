import { Component, OnInit } from '@angular/core';
import {DCheroEvents} from "../../models/DCheroEvents";

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  // properties
  characters: DCheroEvents[];
  enableAddUser: boolean;
  currentClasses: {};
  currentStyle: {};
  showForm: boolean;

  constructor() { }

  ngOnInit(): void {
    this.enableAddUser = true;
    this.showForm = false;
    // adding data of array of characters
    this.characters = [{
      persona: 'Superman',
      firstName: 'Clark',
      lastName: 'Kent',
      age: 34,
      address: {
        street: '27 Smallville',
        city: 'Metropolis',
        state: 'IL'
      },
      img: '../../assets/img/superman.jpg',
      isActive: true,
      balance: 123000,
      memberSince: new Date('05/01/1939 8:30:00'),
      hide: false
    },
      {
        persona: 'Raven',
        firstName: 'Rachel',
        lastName: 'Roth',
        age: 134,
        address: {
          street: '1600 Main St',
          city: 'Los Angeles',
          state: 'CA'
        },
        img: '../../assets/img/raven.jpg',
        hide: false
      },
      {
        persona: 'Batman',
        firstName: 'Bruce',
        lastName: 'Wayne',
        age: 43,
        address: {
          street: '50 Wayne Manor',
          city: 'Gotham',
          state: 'NY'
        },
        img: '../../assets/img/batman.jpg',
        isActive: true,
        memberSince: new Date('01/01/1937 8:30:00'),
        hide: false
      }] // end or array

    this.setCurrentClasses();
    this.setCurrentStyle()
  }

  // Method
  // ngClass
  setCurrentClasses() {
    this.currentClasses = {
      'btn-success': this.enableAddUser
    }
  }

  // ngStyle
  setCurrentStyle() {
    this.currentStyle ={
      'padding-top': '60px',
      'text-decoration': 'underline'
    }
  }

  toggleForm() {
    this.showForm = !this.showForm
  }

  toggleInfo(character) {
    // alert('button was clicked');
    character.hide = !character.hide
  }

  fireEvent(e) {
    console.log(e.type);
    console.log(e.target.value)
  }

  // toggleBalance(character){
  //   // console.log(character.balance);
  //   if (character.balance == undefined){
  //     character.balance = 3.5
  //     alert('Bout three fiddy')
  //   }
  //   character.balance += 1000;
  // }

  // methods for the type of events
  triggerEvent(e) {
    console.log(e.type)
  }

}
